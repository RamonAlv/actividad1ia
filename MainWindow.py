from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import cv2 as cv
import numpy as np
import threading
import time

class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Computer Vision")
        self.leftPanel = QVBoxLayout()
        self.btnLoadImages = QPushButton("Cargar imágenes")
        self.btnSelectROI = QPushButton("Seleccionar ROI")
        self.btnExecute = QPushButton("Ejecutar")
        self.algorithmLabel = QLabel("Algoritmo de búsqueda:")
        self.alphaLabel = QLabel("Alfa:")
        self.algorithmComboBox = QComboBox()
        self.alphaSpinBox = QDoubleSpinBox()
        self.vSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.leftPanel.addWidget(self.btnLoadImages)
        self.leftPanel.addWidget(self.btnSelectROI)
        self.leftPanel.addWidget(self.algorithmLabel)
        self.leftPanel.addWidget(self.algorithmComboBox)
        self.leftPanel.addWidget(self.alphaLabel)
        self.leftPanel.addWidget(self.alphaSpinBox)
        self.leftPanel.addSpacerItem(self.vSpacer)
        self.leftPanel.addWidget(self.btnExecute)
        self.horizontalLayout = QHBoxLayout()
        self.hSpacer1 = QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.hSpacer2 = QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.vSpacer1 = QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.vSpacer2 = QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.imageLabel = QLabel()
        self.progressBar = QProgressBar()
        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.addSpacerItem(self.vSpacer1)
        self.verticalLayout.addWidget(self.imageLabel)
        self.verticalLayout.addSpacerItem(self.vSpacer2)
        self.horizontalLayout.addSpacerItem(self.hSpacer1)
        self.horizontalLayout.addLayout(self.verticalLayout)
        self.horizontalLayout.addSpacerItem(self.hSpacer2)
        self.mainHorizontalLayout = QHBoxLayout()
        self.mainHorizontalLayout.addLayout(self.leftPanel)
        self.mainHorizontalLayout.addLayout(self.horizontalLayout)
        self.mainVerticalLayout = QVBoxLayout()
        self.mainVerticalLayout.addLayout(self.mainHorizontalLayout)
        self.mainVerticalLayout.addWidget(self.progressBar)
        self.setLayout(self.mainVerticalLayout)
        self.btnSelectROI.setEnabled(False)
        self.btnExecute.setEnabled(False)
        algorithms = ["Sum of Squared Diferences",
                      "Sum of Absolute Diferences",
                      "Cross Correlation"]
        self.algorithmComboBox.addItems(algorithms)
        self.alphaSpinBox.setRange(0.01, 0.9)
        self.alphaSpinBox.setSingleStep(0.1)
        self.btnLoadImages.clicked.connect(self.on_btnLoadImages_clicked)
        self.btnSelectROI.clicked.connect(self.on_btnSelectROI_clicked)
        self.btnExecute.clicked.connect(self.on_btnExecute_clicked)

    def on_btnLoadImages_clicked(self):
        self.files, _ = QFileDialog.getOpenFileNames(
                            self,
                            "Selecciona el conjunto de imágenes",
                            "/home/emmanuel/Escritorio/Artificial_Intelligence/Corte1/Assignment1/",
                            "Archivos de Imagen (*.png *.jpg *.jpeg *.bmp)")

        if len(self.files) < 3:
            QMessageBox.information(self, "Info", "Debe cargar al menos 3 imágenes.")
        else:
            qImage = QImage(self.files[0])
            pixmap = QPixmap.fromImage(qImage)
            self.imageLabel.setPixmap(pixmap)
            self.step = 100 / (len(self.files) - 1)
            self.btnSelectROI.setEnabled(True)

    def on_btnSelectROI_clicked(self):
        image = cv.imread(self.files[0])
        self.ROI_x, self.ROI_y, self.ROI_width, self.ROI_height = cv.selectROI(image, False, False)
        cv.destroyAllWindows()

        if self.ROI_width > 0 and self.ROI_height > 0:
            image = cv.imread(self.files[0], 0)
            self.ROI = image[self.ROI_y:self.ROI_y+self.ROI_height, self.ROI_x:self.ROI_x+self.ROI_width]

            self.qImage = QImage(self.files[0])
            self.pixmap = QPixmap.fromImage(self.qImage)
            self.painter = QPainter()
            self.painter.begin(self.pixmap)
            self.painter.setPen(Qt.green)
            self.painter.drawRect(self.ROI_x, self.ROI_y, self.ROI_width, self.ROI_height)
            print("X__>",self.ROI_x,"ROI W +++++>", self.ROI_width ,"Y-----> ", self.ROI_y,"ROI H      >", self.ROI_height)
            self.painter.end()
            self.imageLabel.setPixmap(self.pixmap)
            self.btnExecute.setEnabled(True)

    def on_btnExecute_clicked(self):
        algorithm = self.algorithmComboBox.currentText()

        #alpha = round(self.alphaSpinBox.value(), 1)
        #beta = round(1 - alpha, 1)
        alpha = self.alphaSpinBox.value()
        beta = 1 - alpha

        self.background = cv.imread(self.files[0])

        init_time = time.time()
        #Temporales
        IMG = cv.imread(self.files[0], 0)
        self.Inicialy = self.ROI_y-50
        self.Finaly = self.ROI_y + self.ROI_height + 50
        self.Inicialx = self.ROI_x-50
        self.Finalx = self.ROI_x + self.ROI_width + 50
        if self.Inicialy < 0:
            self.Inicialy = self.Inicialy + abs(self.Inicialy)
        if self.Finaly > IMG.shape[0]:
            self.Finaly = IMG.shape[0]
        if self.Inicialx < 0:
            self.Inicialx = self.Inicialx + abs(self.Inicialx)
        if self.Finalx > IMG.shape[1]:
            self.Finalx = IMG.shape[1]
        self.pixmap = QPixmap.fromImage(self.qImage)
        self.painter = QPainter()
        self.painter.begin(self.pixmap)
        self.painter.setPen(Qt.red)
        self.painter.drawRect(self.Inicialx, self.Inicialy, self.Finalx, self.Finaly)
        self.painter.end()
        self.imageLabel.setPixmap(self.pixmap)
        self.btnExecute.setEnabled(True)
        #Temporales

        for i in range(1, len(self.files)):
            if algorithm == "Sum of Squared Diferences":
                aligned_image = self.SSD(i)
            elif algorithm == "Sum of Absolute Diferences":
                aligned_image = self.SAD(i)
            else:
                aligned_image = self.CC(i)

            self.background = beta*self.background + alpha*aligned_image

            cv.imwrite("Background{}.jpg".format(i), self.background)
            threading.Thread(target=update_interface, args=(self.imageLabel, self.progressBar, self.step*i, i)).start()

        elapsed_time = time.time() - init_time
        print("Duración:", elapsed_time, "segundos")

    def SSD(self, index):
        """ Returns the Sum of Squared Differences Value
        """
        shifted_image = cv.imread(self.files[index], 0)

        min = 99**99
        pos = []
        for y in range(self.Inicialy , self.Finaly-self.ROI_height):
            for x in range(self.Inicialx , self.Finalx-self.ROI_width):
                sum = 0
                temp_matrix = shifted_image[y:y+self.ROI_height, x:x+self.ROI_width]
                sum = np.sum((self.ROI - temp_matrix) ** 2)
                if sum < min:
                    min = sum
                    pos = [x, y]

        deltaX = self.ROI_x - pos[0]
        deltaY = self.ROI_y - pos[1]

        # print("X:", deltaX, "Y:", deltaY)

        shifted_image = cv.imread(self.files[index])

        transform_matrix = np.float32([[1,0,deltaX],[0,1,deltaY]])
        aligned_image = cv.warpAffine(shifted_image, transform_matrix, (shifted_image.shape[1], shifted_image.shape[0]))

        return aligned_image

    def SAD(self, index):
        """ Returns the Sum of Absolute Differences Value
        """
        shifted_image = cv.imread(self.files[index], 0)

        min = 99**99
        pos = []

        for y in range(self.Inicialy , self.Finaly-self.ROI_height):
            for x in range(self.Inicialx , self.Finalx-self.ROI_width):
                sum = 0
                temp_matrix = shifted_image[y:y+self.ROI_height, x:x+self.ROI_width]
                
                sum = np.sum(abs(self.ROI - temp_matrix))
                if sum < min:
                    min = sum
                    pos = [x, y]

        deltaX = self.ROI_x - pos[0]
        deltaY = self.ROI_y - pos[1]

        shifted_image = cv.imread(self.files[index])

        transform_matrix = np.float32([[1,0,deltaX],[0,1,deltaY]])
        aligned_image = cv.warpAffine(shifted_image, transform_matrix, (shifted_image.shape[1], shifted_image.shape[0]))

        return aligned_image

    def CC(self, index):
        """ Returns the Cross Correlation Value
        """
        shifted_image = cv.imread(self.files[index], 0)

        min = 99**99
        pos = []

        for y in range(self.Inicialy , self.Finaly-self.ROI_height):
            for x in range(self.Inicialx , self.Finalx-self.ROI_width):
                sum = 0
                temp_matrix = shifted_image[y:y+self.ROI_height, x:x+self.ROI_width]
                sum = np.sum(self.ROI * temp_matrix)
                if sum < min:
                    min = sum
                    pos = [x, y]

        deltaX = self.ROI_x - pos[0]
        deltaY = self.ROI_y - pos[1]

        shifted_image = cv.imread(self.files[index])

        transform_matrix = np.float32([[1,0,deltaX],[0,1,deltaY]])
        aligned_image = cv.warpAffine(shifted_image, transform_matrix, (shifted_image.shape[1], shifted_image.shape[0]))

        return aligned_image

def update_interface(label, progressBar, progress, i):
    qImage = QImage("Background{}.jpg".format(i))
    pixmap = QPixmap.fromImage(qImage)
    label.setPixmap(pixmap)
    progressBar.setValue(progress)
